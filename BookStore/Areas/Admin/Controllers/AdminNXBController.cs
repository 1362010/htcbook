﻿using BookStore.Areas.Admin.ViewModels;
using BookStoreBus.Bus;
using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace BookStore.Areas.Admin.Controllers
{
    public class AdminNXBController : Controller
    {
        //[Authorize(Roles = "Admin")]
        [Authorize(Roles = "Admin,Manager,Employee")]
        //
        // GET: /Admin/AdminNXB/
        public ActionResult Index()
        {
            return View(NXBBus.DanhSach());
        }

        //
        // GET: /Admin/AdminNXB/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Admin/AdminNXB/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/AdminNXB/Create
        [HttpPost]
        public ActionResult Create(NXB nxb)
        {
            try
            {
                // TODO: Add insert logic here
                BookStoreConnectionDB.GetInstance().Insert(nxb);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/AdminNXB/Edit/5
        public ActionResult Edit(int id)
        {
            return View(NXBBus.GetByID2(id));
        }

        //
        // POST: /Admin/AdminNXB/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, NXB nxb)
        {
            try
            {
                // TODO: Add update logic here
                BookStoreConnectionDB.GetInstance().Update(nxb);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/AdminNXB/Delete/5
        public ActionResult Delete(int id)
        {
            return View(NXBBus.GetByID2(id));
        }

        //
        // POST: /Admin/AdminNXB/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, NXB nxb)
        {
            try
            {
                // TODO: Add delete logic here
                BookStoreConnectionDB.GetInstance().Delete<NXB>(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
