﻿using BookStore.Areas.Admin.ViewModels;
using BookStoreBus.Bus;
using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using PetaPoco;

namespace BookStore.Areas.Admin
{
    public class AdminUSERController : Controller
    {
        //[Authorize(Roles = "Admin")]
       [Authorize(Roles = "Admin,Manager")]
        //
        // GET: /Admin/AdminUSER/
        public ActionResult Index(int? page)
        {
            if (page == null)
                page = 1;
            BookStoreConnectionDB db = new BookStoreConnectionDB();
            Page<ViewUser> dsuser = db.Page<ViewUser>(page.Value, 10, "select *from ViewUser"); 
            return View(dsuser);
        }

        public ActionResult DSU(int? page)
        {
             if (page == null)
                 page = 1;
             BookStoreConnectionDB db = new BookStoreConnectionDB();
             Page<AspNetUser> dsuser = db.Page<AspNetUser>(page.Value, 10, "select *from AspNetUsers");
             return View(dsuser);
        }

        //
        // GET: /Admin/AdminUSER/Details/5
        public ActionResult Details(string id)
        {
            return View(USERBus.GetByID(id));
        }

        //
        // GET: /Admin/AdminUSER/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/AdminUSER/Create
        [HttpPost]
        public ActionResult Create(ViewUser User)
        {
            try
            {
                // TODO: Add insert logic here
                BookStoreConnectionDB.GetInstance().Insert(User);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/AdminUSER/Edit/5
        public ActionResult Edit(string id)
        {
            return View(USERBus.GetByID(id));
        }

        //
        // POST: /Admin/AdminUSER/Edit/5
        [HttpPost]
        public ActionResult Edit(string id, AspNetUserRole User)
        {
            try
            {
                // TODO: Add update logic here
                BookStoreConnectionDB.GetInstance().Update(User);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit2(string id)
        {
            return View(USERBus.GetByID2(id));
        }

        //
        // POST: /Admin/AdminUSER/Edit/5
        [HttpPost]
        public ActionResult Edit2(string id, AspNetUser User)
        {
            try
            {
                // TODO: Add update logic here
                BookStoreConnectionDB.GetInstance().Update(User);
                return RedirectToAction("DSU");
            }
            catch
            {
                return View();
            }
        }
        //
        // GET: /Admin/AdminUSER/Delete/5
        public ActionResult Delete(string id)
        {
            BookStoreConnectionDB db = new BookStoreConnectionDB();

            return View(db.SingleOrDefault<AspNetUser>("select *from AspNetUsers where Id=@0", id));
        }

        //
        // POST: /Admin/AdminUSER/Delete/5
        [HttpPost]
        public ActionResult Delete(string id, AspNetUser user)
        {
            try
            {
                // TODO: Add delete logic here
                BookStoreConnectionDB.GetInstance().Delete<AspNetUser>(user);
                return RedirectToAction("DSU");
            }
            catch
            {
                return View();
            }
        }
    }
}
