﻿using BookStore.Areas.Admin.ViewModels;
using BookStoreBus.Bus;
using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace BookStore.Areas.Admin.Controllers
{
    //[Authorize(Roles = "Admin")]
        [Authorize(Roles = "Admin,Manager,Employee")]
    public class AdminCTPHController : Controller
    {
        //
        // GET: /Admin/AdminCTPH/
        public ActionResult Index()
        {
            return View(CTPHBus.DanhSach());
        }

        //
        // GET: /Admin/AdminCTPH/Details/5
        public ActionResult Details(int id)
        {
            return View(CTPHBus.GetByID(id));
        }

        //
        // GET: /Admin/AdminCTPH/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/AdminCTPH/Create
        [HttpPost]
        public ActionResult Create(CTYPHATHANH CTPH)
        {
            try
            {
                // TODO: Add insert logic here
                BookStoreConnectionDB.GetInstance().Insert(CTPH);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/AdminCTPH/Edit/5
        public ActionResult Edit(int id)
        {
            return View(CTPHBus.GetByID2(id));
        }

        //
        // POST: /Admin/AdminCTPH/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CTYPHATHANH CTPH)
        {
            try
            {
                // TODO: Add update logic here
                BookStoreConnectionDB.GetInstance().Update(CTPH);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/AdminCTPH/Delete/5
        public ActionResult Delete(int id)
        {
            return View(CTPHBus.GetByID2(id));
        }

        //
        // POST: /Admin/AdminCTPH/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, CTYPHATHANH CTPH)
        {
            try
            {
                // TODO: Add delete logic here
                BookStoreConnectionDB.GetInstance().Delete<CTYPHATHANH>(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
