﻿using BookStore.Areas.Admin.ViewModels;
using BookStoreBus.Bus;
using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace BookStore.Areas.Admin.Controllers
{
    public class AdminHOADONController : Controller
    {
        //[Authorize(Roles = "Admin")]
        [Authorize(Roles = "Admin,Manager,Employee")]
        //
        // GET: /Admin/AdminHOADON/
        public ActionResult Index()
        {
            return View(HOADONBus.DanhSach());
        }

        public ActionResult DHChuaGiao()
        {
            return View(HOADONBus.DanhSach2());
        }
        //
        // GET: /Admin/AdminHOADON/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Admin/AdminHOADON/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/AdminHOADON/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/AdminHOADON/Edit/5
        public ActionResult Edit(int id)
        {
            return View(HOADONBus.GetByID3(id));
        }

        //
        // POST: /Admin/AdminHOADON/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, HOADON HD)
        {
            try
            {
                // TODO: Add update logic here
                BookStoreConnectionDB.GetInstance().Update(HD);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit2(int id)
        {
            return View(HOADONBus.GetByID3(id));
        }

        //
        // POST: /Admin/AdminHOADON/Edit/5
        [HttpPost]
        public ActionResult Edit2(int id, HOADON HD)
        {
            try
            {
                // TODO: Add update logic here
                BookStoreConnectionDB.GetInstance().Update(HD);
                return RedirectToAction("DHChuaGiao");
            }
            catch
            {
                return View();
            }
        }
        //
        // GET: /Admin/AdminHOADON/Delete/5
        public ActionResult Delete(int id)
        {
            return View(HOADONBus.GetByID3(id));
        }

        //
        // POST: /Admin/AdminHOADON/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, HOADON hoadon)
        {
            try
            {
                // TODO: Add delete logic here
                BookStoreConnectionDB.GetInstance().Delete<HOADON>(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
