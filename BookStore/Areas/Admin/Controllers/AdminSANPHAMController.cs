﻿using BookStore.Areas.Admin.ViewModels;
using BookStoreBus.Bus;
using BookStoreConnection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace BookStore.Areas.Admin.Controllers
{
    //[Authorize(Roles = "Admin")]
    [Authorize(Roles = "Admin,Manager,Employee")]
    public class AdminSANPHAMController : Controller
    {
        public ActionResult Index(int? page)
        {
            if (page == null)
                page = 1;
            BookStoreConnectionDB db = new BookStoreConnectionDB();
            Page<ViewSANPHAM> dsSP = db.Page<ViewSANPHAM>(page.Value, 10, "select * from ViewSANPHAM ORDER BY TenSP ASC");
            return View(dsSP);  
        }
        public ActionResult Details(int id)
        {

            return View(SANPHAMBus.GetByID(id));
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(SANPHAMViewModel SP)
        {

            var validImageTypes = new string[]
            {
                "image/gif",
                "image/jpeg",
                "image/pjpeg",
                "image/png"
            };

            if (SP.ImageUpload == null || SP.ImageUpload.ContentLength == 0)
            {
                ModelState.AddModelError("ImageUpload", "This field is required");
            }
            else if (!validImageTypes.Contains(SP.ImageUpload.ContentType))
            {
                ModelState.AddModelError("ImageUpload", "Please choose either a GIF, JPG or PNG image.");
            }

            if (ModelState.IsValid)
            {
                
                
                
                if (SP.ImageUpload != null && SP.ImageUpload.ContentLength > 0)
                {
                    var uploadDir = "~/img";
                   var imagePath = Path.Combine(Server.MapPath(uploadDir),SP.ImageUpload.FileName);
                    SP.ImageUpload.SaveAs(imagePath);
                    SP.HinhAnh = SP.ImageUpload.FileName;
                }
                BookStoreConnectionDB.GetInstance().Insert(SP);
                return RedirectToAction("Index");
            }

            return View();
        }

        public ActionResult Edit(int id)
        {
            return View(SANPHAMBus.GetByID(id));
        }

        [HttpPost]
        public ActionResult Edit(int id, SANPHAM SANPHAM)
        {
            try
            {
                BookStoreConnectionDB.GetInstance().Update(SANPHAM);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult Delete(int id)
        {
            return View(SANPHAMBus.GetByID(id));
        }
        [HttpPost]
        public ActionResult Delete(int id, SANPHAM SANPHAM)
        {
            try
            {
                // TODO: Add delete logic here
                BookStoreConnectionDB.GetInstance().Delete<SANPHAM>(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
