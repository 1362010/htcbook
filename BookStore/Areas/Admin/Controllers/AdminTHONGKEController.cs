﻿using BookStore.Areas.Admin.ViewModels;
using BookStoreBus.Bus;
using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace BookStore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,Manager,Employee")]
    public class AdminTHONGKEController : Controller
    {
        //
        // GET: /Admin/AdminTHONGKE/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Ngay()
        {
            return View();
        }

        public ActionResult Tuan()
        {
            return View();
        }

        public ActionResult Thang()
        {
            return View();
        }

        public ActionResult Quy()
        {
            return View();
        }

        public ActionResult Nam()
        {
            return View();
        }

        public ActionResult TongDoanhThu()
        {
            return View();
        }

        public ActionResult Top10SP()
        {
            return View(SANPHAMBus.ThongKeTop10SP());
        }
    }
}
