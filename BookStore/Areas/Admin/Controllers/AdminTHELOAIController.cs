﻿using BookStore.Areas.Admin.ViewModels;
using BookStoreBus.Bus;
using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace BookStore.Areas.Admin.Controllers
{
    //[Authorize(Roles = "Admin")]
        [Authorize(Roles = "Admin,Manager,Employee")]
    public class AdminTHELOAIController : Controller
    {
        //
        // GET: /Admin/AdminTHELOAI/
        public ActionResult Index()
        {
            return View(THELOAIBus.DanhSach());
        }

        //
        // GET: /Admin/AdminTHELOAI/Details/5
        public ActionResult Details(int id)
        {
            return View(THELOAIBus.GetByID(id));
        }

        //
        // GET: /Admin/AdminTHELOAI/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/AdminTHELOAI/Create
        [HttpPost]
        public ActionResult Create(THELOAI tl)
        {
            try
            {
                // TODO: Add insert logic here
                BookStoreConnectionDB.GetInstance().Insert(tl);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/AdminTHELOAI/Edit/5
        public ActionResult Edit(int id)
        {
            return View(THELOAIBus.GetByID2(id));
        }

        //
        // POST: /Admin/AdminTHELOAI/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, THELOAI tl)
        {
            try
            {
                // TODO: Add update logic here
                BookStoreConnectionDB.GetInstance().Update(tl);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/AdminTHELOAI/Delete/5
        public ActionResult Delete(int id)
        {
            return View(THELOAIBus.GetByID2(id));
        }

        //
        // POST: /Admin/AdminTHELOAI/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, THELOAI tl)
        {
            try
            {
                // TODO: Add delete logic here
                BookStoreConnectionDB.GetInstance().Delete<THELOAI>(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
