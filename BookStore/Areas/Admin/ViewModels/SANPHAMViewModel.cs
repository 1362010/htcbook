﻿using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Areas.Admin.ViewModels
{
    public class SANPHAMViewModel : SANPHAM
    {
        [Display(Name="Hình ảnh của sản phẩm")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase ImageUpload { get; set; }

        public string ImageUrl { get; set; }

        [Display(Name="Tên sản phẩm")]
        public string TenSPLable { get; set; }

        [Display(Name = "Tác giả")]
        public string TacGiaLable { get; set; }

        [Display(Name = "Kích thước")]
        public string KichThuocLable { get; set; }

        [Display(Name = "Trọng lượng")]
        public string TrongLuongLable { get; set; }

        [Display(Name = "Ngày phát hành")]
        public string NgayPhatHanhLable { get; set; }

        [Display(Name = "Giá")]
        public string GiaLable { get; set; }

        
        [Display(Name = "Mô tả")]
        public string MoTaLable { get; set; }

        [Display(Name = "Lượt xem")]
        public string LuotXemLable { get; set; }

        [Display(Name = "Số lượng nhập")]
        public string SLNhapLable { get; set; }

        [Display(Name = "Số lượng bán")]
        public string SLBanLable { get; set; }

        [Display(Name = "Thể loại")]
        public string MaTheLoaiLable { get; set; }

        [Display(Name = "Cty phát hành")]
        public string MaCTPHLable { get; set; }

        [Display(Name = "Nhà xuất bản")]
        public string MaNhaXuatBanLable { get; set; }

        [Display(Name = "Tình trang sản phẩm")]
        public string MaTTSPLable { get; set; }

    }
}