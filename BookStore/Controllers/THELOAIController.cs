﻿using BookStoreBus.Bus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class THELOAIController : Controller
    {
        // GET: THELOAI
        public ActionResult Index(int id)
        {
            return View(THELOAIBus.GetByID(id));
        }

        // GET: THELOAI/Details/5
        public ActionResult Details()
        {
            return View();
        }

        // GET: THELOAI/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: THELOAI/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: THELOAI/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: THELOAI/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: THELOAI/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: THELOAI/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
