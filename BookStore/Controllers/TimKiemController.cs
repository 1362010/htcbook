﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;
using BookStoreBus.Bus;
using BookStoreConnection;
using PetaPoco;

namespace BookStore.Controllers
{
    public class TimKiemController : Controller
    {
        BookStoreConnectionDB db = new BookStoreConnectionDB();
        //
        // GET: /TimKiem/
        [HttpPost]
        public ActionResult KetQuaTimKiem(FormCollection f,int? page)
        {
            
            string TuKhoa = f["txttimkiem"].ToString().ToUpper();
            ViewBag.tk = TuKhoa;
            string timtheo = f["timtheo"].ToString();
            ViewBag.tt = timtheo;
            if (timtheo == "0" || timtheo == "1")
            {
                List<ViewSANPHAM> lstKQTK = db.Query<ViewSANPHAM>("select * from ViewSANPHAM ").Where<ViewSANPHAM>(n => n.TenSP.ToUpper().Contains(TuKhoa)).ToList();

                ViewBag.kq = lstKQTK.Count;

                int pageNumber = (page ?? 1);
                int pageSize = 9;
                if (lstKQTK.Count == 0)
                {
                    ViewBag.ThongBao = "không tìm thấy";

                }
                else
                {
                    ViewBag.ThongBao = "tìm thấy " + lstKQTK.Count + " kết quả!";
                    return View(lstKQTK.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
                }
            }
            if (timtheo == "2")
            {
                List<ViewSANPHAM> lstKQTK = db.Query<ViewSANPHAM>("select * from ViewSANPHAM ").Where<ViewSANPHAM>(n => n.TenTheLoai.ToUpper().Contains(TuKhoa)).ToList();

                ViewBag.kq = lstKQTK.Count;

                int pageNumber = (page ?? 1);
                int pageSize = 9;
                if (lstKQTK.Count == 0)
                {
                    ViewBag.ThongBao = "không tìm thấy";

                }
                else
                {
                    ViewBag.ThongBao = "tìm thấy " + lstKQTK.Count + " kết quả!";
                    return View(lstKQTK.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
                }
            }
            if (timtheo == "3")
            {
                List<ViewSANPHAM> lstKQTK = db.Query<ViewSANPHAM>("select * from ViewSANPHAM ").Where<ViewSANPHAM>(n => n.TenNXB.ToUpper().Contains(TuKhoa)).ToList();

                ViewBag.kq = lstKQTK.Count;

                int pageNumber = (page ?? 1);
                int pageSize = 9;
                if (lstKQTK.Count == 0)
                {
                    ViewBag.ThongBao = "không tìm thấy";

                }
                else
                {
                    ViewBag.ThongBao = "tìm thấy " + lstKQTK.Count + " kết quả!";
                    return View(lstKQTK.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
                }
            }
            if (timtheo == "4")
            {
                List<ViewSANPHAM> lstKQTK = db.Query<ViewSANPHAM>("select * from ViewSANPHAM ").Where<ViewSANPHAM>(n => n.TacGia.ToUpper().Contains(TuKhoa)).ToList();

                ViewBag.kq = lstKQTK.Count;

                int pageNumber = (page ?? 1);
                int pageSize = 9;
                if (lstKQTK.Count == 0)
                {
                    ViewBag.ThongBao = "không tìm thấy";

                }
                else
                {
                    ViewBag.ThongBao = "tìm thấy " + lstKQTK.Count + " kết quả!";
                    return View(lstKQTK.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult KetQuaTimKiem(string TuKhoa, int? page,string timtheo)
        {

            ViewBag.tk = TuKhoa;
            ViewBag.tt = timtheo;
            if (timtheo == "1")
            {
                List<ViewSANPHAM> lstKQTK = db.Query<ViewSANPHAM>("select * from ViewSANPHAM ").Where<ViewSANPHAM>(n => n.TenSP.ToUpper().Contains(TuKhoa)).ToList();

                ViewBag.kq = lstKQTK.Count;
                int pageNumber = (page ?? 1);
                int pageSize = 9;
                if (lstKQTK.Count == 0)
                {
                    ViewBag.ThongBao = "Không tìm thấy";

                }
                else
                {
                    ViewBag.ThongBao = "tìm thấy " + lstKQTK.Count + " kết quả!";

                }
                return View(lstKQTK.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
            }
            if (timtheo == "2")
            {
                List<ViewSANPHAM> lstKQTK = db.Query<ViewSANPHAM>("select * from ViewSANPHAM ").Where<ViewSANPHAM>(n => n.TenTheLoai.ToUpper().Contains(TuKhoa)).ToList();

                ViewBag.kq = lstKQTK.Count;
                int pageNumber = (page ?? 1);
                int pageSize = 9;
                if (lstKQTK.Count == 0)
                {
                    ViewBag.ThongBao = "Không tìm thấy";

                }
                else
                {
                    ViewBag.ThongBao = "tìm thấy " + lstKQTK.Count + " kết quả!";

                }
                return View(lstKQTK.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
            }
            if (timtheo == "3")
            {
                List<ViewSANPHAM> lstKQTK = db.Query<ViewSANPHAM>("select * from ViewSANPHAM ").Where<ViewSANPHAM>(n => n.TenNXB.ToUpper().Contains(TuKhoa)).ToList();

                ViewBag.kq = lstKQTK.Count;
                int pageNumber = (page ?? 1);
                int pageSize = 9;
                if (lstKQTK.Count == 0)
                {
                    ViewBag.ThongBao = "Không tìm thấy";

                }
                else
                {
                    ViewBag.ThongBao = "tìm thấy " + lstKQTK.Count + " kết quả!";

                }
                return View(lstKQTK.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
            }
            if (timtheo == "4")
            {
                List<ViewSANPHAM> lstKQTK = db.Query<ViewSANPHAM>("select * from ViewSANPHAM ").Where<ViewSANPHAM>(n => n.TacGia.ToUpper().Contains(TuKhoa)).ToList();

                ViewBag.kq = lstKQTK.Count;
                int pageNumber = (page ?? 1);
                int pageSize = 9;
                if (lstKQTK.Count == 0)
                {
                    ViewBag.ThongBao = "Không tìm thấy";

                }
                else
                {
                    ViewBag.ThongBao = "tìm thấy " + lstKQTK.Count + " kết quả!";

                }
                return View(lstKQTK.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
            }
            return View();
        }
        
    }
}
