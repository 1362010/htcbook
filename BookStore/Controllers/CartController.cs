﻿using BookStoreBus.Bus;
using BookStoreConnection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookStore.Models;

namespace BookStore.Controllers
{
    public class CartController : Controller
    {
        public ActionResult ShoppingCart()
        {
            return View();
        }

        
        public ActionResult Checkout()
        {
            return View();
        }
        public ActionResult Success()
        {
            return View();
        }

        [HttpPost]
       
        public ActionResult Checkout(HOADON HD)
        {
            try
            {
                BookStoreConnectionDB.GetInstance().Insert(HD);
                EmptyAll();
            }
            catch
            {
                return View();
            }
            return RedirectToAction("Success", "Cart");
        }

        [HttpPost]
        public JsonResult AddToCart(int id)
        {
            List<CartItem> listCartItem;
            if (Session["ShoppingCart"] == null)
            {
                listCartItem = new List<CartItem>();
                listCartItem.Add(new CartItem { Quality = 1, productOrder = CARTBus.AddToCart(id) });
                Session["ShoppingCart"] = listCartItem;
            }
            else
            {
                bool flag = false;
                listCartItem = (List<CartItem>)Session["ShoppingCart"];
                foreach (CartItem item in listCartItem)
                {
                    if (item.productOrder.MaSP == id)
                    {
                        item.Quality++;
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                    listCartItem.Add(new CartItem { Quality = 1, productOrder = CARTBus.AddToCart(id) });
                Session["ShoppingCart"] = listCartItem;
            }
            int cartcount = 0;
            foreach (CartItem item in listCartItem)
            {
                cartcount += item.Quality;
            }
            return Json(new { ItemAmount = cartcount });
        }


        [HttpPost]
        public JsonResult Remove(int id)
        {
            List<CartItem> listCartItem;
            if (Session["ShoppingCart"] == null)
            {
                listCartItem = new List<CartItem>();
                listCartItem.Add(new CartItem { Quality = 1, productOrder = CARTBus.AddToCart(id) });
                Session["ShoppingCart"] = listCartItem;
            }
            else
            {
                bool flag = false;
                listCartItem = (List<CartItem>)Session["ShoppingCart"];
                foreach (CartItem item in listCartItem)
                {
                    if (item.productOrder.MaSP == id)
                    {
                        item.Quality--;
                        if(item.Quality<=0)
                        {
                            Delete(id);
                        }
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                    listCartItem.Add(new CartItem { Quality = 1, productOrder = CARTBus.AddToCart(id) });
                Session["ShoppingCart"] = listCartItem;
            }
            int cartcount = 0;
            foreach (CartItem item in listCartItem)
            {
                cartcount += item.Quality;
            }
            return Json(new { ItemAmount = cartcount });
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            List<CartItem> listCartItem;
            bool flag = false;
            listCartItem = (List<CartItem>)Session["ShoppingCart"];
            foreach (CartItem item in listCartItem)
            {
                if (item.productOrder.MaSP == id)
                {
                    listCartItem.Remove(item);
                    flag = true;
                    break;
                }
            }
            if (!flag)
                listCartItem.Add(new CartItem { Quality = 1, productOrder = CARTBus.AddToCart(id) });
            Session["ShoppingCart"] = listCartItem;
            int cartcount = 0;
            foreach (CartItem item in listCartItem)
            {
                cartcount += item.Quality;
            }
            return Json(new { ItemAmount = cartcount });
        }

        [HttpPost]
        public JsonResult EmptyAll()
        {
            Session["ShoppingCart"] = null;
            return Json(new { ItemAmount = 0 });
        }

        [HttpPost]
        public JsonResult Update(int id, int Quality)
        {
            List<CartItem> listCartItem;
            bool flag = false;
            listCartItem = (List<CartItem>)Session["ShoppingCart"];
            foreach (CartItem item in listCartItem)
            {
                if (item.productOrder.MaSP == id)
                {
                    item.Quality = Quality;
                    flag = true;
                    break;
                }
            }
            if (!flag)
                listCartItem.Add(new CartItem { Quality = 1, productOrder = CARTBus.AddToCart(id) });
            Session["ShoppingCart"] = listCartItem;
            int cartcount = 0;
            foreach (CartItem item in listCartItem)
            {
                cartcount += item.Quality;
            }
            return Json(new { ItemAmount = cartcount });
        }
	}
}