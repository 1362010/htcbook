﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookStoreBus.Bus;

namespace BookStore.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu
        public ActionResult Main()
        {
            return View(THELOAIBus.DanhSach());
        }
        public ActionResult NXB()
        {
            return View(NXBBus.DanhSach());
        }
        public ActionResult CTPH()
        {
            return View(CTPHBus.DanhSach());
        }
    }
}