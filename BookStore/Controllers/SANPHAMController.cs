﻿using BookStoreBus.Bus;
using BookStoreConnection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class SANPHAMController : Controller
    {
        //
        // GET: /SANPHAM/
        public ActionResult Index(int? page)
        {
            if(page==null)
                page = 1;
            
            BookStoreConnectionDB db=new BookStoreConnectionDB();
            Page<ViewSANPHAM> dsSP = db.Page<ViewSANPHAM>(page.Value, 9, "select * from ViewSANPHAM");
            return View(dsSP);
        }

        //
        // GET: /SANPHAM/Details/5
        public ActionResult Details(int id)
        {
            return View(SANPHAMBus.GetByID(id));
        }

        //
        // GET: /SANPHAM/Create
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /SANPHAM/Create
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /SANPHAM/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /SANPHAM/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /SANPHAM/Delete/5
        public ActionResult Delete(int id)
        {
            return View(SANPHAMBus.GetByID(id));
        }

        //
        // POST: /SANPHAM/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult SPLienquan()
        {
            return View(SANPHAMBus.SPLienquan());
        }


    }
}
