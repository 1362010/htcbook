﻿using BookStoreBus.Bus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class HOADONController : Controller
    {
        //
        // GET: /HOADON/
        public ActionResult Index(string id)
        {
            return View(HOADONBus.GetByID(id));
        }

        //
        // GET: /HOADON/Details/5
        public ActionResult Details()
        {
            return View(HOADONBus.DanhSach());
        }

        //
        // GET: /HOADON/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /HOADON/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /HOADON/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /HOADON/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /HOADON/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /HOADON/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
