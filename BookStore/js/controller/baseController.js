﻿var common = {
    inti: function () {
        common.registerEvent();
    },
    registerEvent: function () {
        $('#timkiem').autocomplete({
            minLength: 0,
            source: function( request, response ) {
                $.ajax({
                    url: "/SANPHAM/ListName",
                    dataType: "jsonp",
                    data: {
                        q: request.term
                    },
                    success: function( res) {
                        response( res.data );
                    }
                });
            },
            focus: function( event, ui ) {
                $( "#timkiem" ).val( ui.item.label );
                return false;
            },
            select: function (event, ui) {
                $("#timkiem").val(ui.item.label);
                return false;
            }
        })
        .autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
              .append("<a>" + item.label + "<br>" + item.desc + "</a>")
              .appendTo(ul);
        };

    }
}
common.inti();