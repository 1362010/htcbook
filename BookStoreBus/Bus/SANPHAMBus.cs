﻿using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace BookStoreBus.Bus
{
    public class SANPHAMBus
    {
        public static IEnumerable<ViewSANPHAM> List()
        {
            var db = new BookStoreConnectionDB();
            return db.Query<ViewSANPHAM>("select * from ViewSANPHAM ORDER BY MaSP ASC");
        }

        public static ViewSANPHAM GetByID(int id)
        {
            var db = new BookStoreConnectionDB();
            return db.SingleOrDefault<ViewSANPHAM>("select * from ViewSANPHAM where MaSP=@0", id);
        }

        public static IEnumerable<ViewSANPHAM> ThongKeTop10SP()
        {
            var db = new BookStoreConnectionDB();
            return db.Query<ViewSANPHAM>("select * from ViewSANPHAM ORDER BY SLBan DESC");
        }

        public static IEnumerable<ViewSANPHAM> SPLienquan()
        {
            var db = new BookStoreConnectionDB();
            return db.Query<ViewSANPHAM>("select * from ViewSANPHAM ORDER BY NEWID()");
        }
    }
}
