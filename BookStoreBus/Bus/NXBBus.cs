﻿using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreBus.Bus
{
    public class NXBBus
    {
        public static IEnumerable<NXB> DanhSach()
        {
            return new BookStoreConnectionDB().Query<NXB>("select * from NXB ORDER BY MaNXB ASC");
        }

        public static IEnumerable<ViewNXB> GetByID(int id)
        {
            var db = new BookStoreConnectionDB();
            return db.Query<ViewNXB>("select * from ViewNXB where MaNXB =@0", id);
        }

        public static NXB GetByID2(int id)
        {
            var db = new BookStoreConnectionDB();
            return db.SingleOrDefault<NXB>("select * from NXB where MaNXB =@0", id);
        }
    }
}
