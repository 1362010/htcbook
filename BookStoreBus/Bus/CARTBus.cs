﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStoreConnection;

namespace BookStoreBus.Bus
{
    public class CARTBus
    {
        public static SANPHAM AddToCart(int id)
        {
            var db = new BookStoreConnectionDB();
            return db.SingleOrDefault<SANPHAM>("select * from SANPHAM where MaSP=@0", id);
        }
    }
}
