﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStoreConnection;

namespace BookStoreBus.Bus
{
    public class CTPHBus
    {
        public static IEnumerable<CTYPHATHANH> DanhSach()
        {
            var db = new BookStoreConnectionDB();
            return db.Query<CTYPHATHANH>("select * from CTYPHATHANH ORDER BY MaCTPH ASC");
        }

        public static IEnumerable<ViewCTPH> GetByID(int id)
        {
            var db = new BookStoreConnectionDB();
            return db.Query<ViewCTPH>("select * from ViewCTPH where MaCTPH =@0", id);
        }

        public static CTYPHATHANH GetByID2(int id)
        {
            var db = new BookStoreConnectionDB();
            return db.SingleOrDefault<CTYPHATHANH>("select * from CTYPHATHANH where MaCTPH=@0", id);
        }
    }
}
