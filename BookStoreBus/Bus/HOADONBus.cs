﻿using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace BookStoreBus.Bus
{
    public class HOADONBus
    {
        public static IEnumerable<HOADON> DanhSach()
        {
            return new BookStoreConnectionDB().Query<HOADON>("select * from HOADON ORDER BY MaHD DESC");
        }
        public static IEnumerable<HOADON> DanhSach2()
        {
            return new BookStoreConnectionDB().Query<HOADON>("select * from HOADON where TinhTrangHD=N'Chưa xác nhận' or TinhTrangHD=N'Đã xác nhận' or TinhTrangHD=N'Chưa giao' ");
        }
        public static IEnumerable<ViewHOADON> GetByID(string id)
        {
            var db = new BookStoreConnectionDB();
            return db.Query<ViewHOADON>("select * from ViewHOADON where MaUser='"+id+"'");
        }

        public static HOADON GetByID2(string id)
        {
            var db = new BookStoreConnectionDB();
            return db.SingleOrDefault<HOADON>("select * from HOADON where MaUser =@0", id);
        }

        public static HOADON GetByID3(int id)
        {
            var db = new BookStoreConnectionDB();
            return db.SingleOrDefault<HOADON>("select * from HOADON where MaHD =@0", id);
        }
    }
}
