﻿using BookStoreConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreBus.Bus
{
    public class USERBus
    {
        public static IEnumerable<ViewUser> DanhSach()
        {
            return new BookStoreConnectionDB().Query<ViewUser>("select * from ViewUser");
        }

        public static AspNetUserRole GetByID(string id)
        {
            var db = new BookStoreConnectionDB();
            return db.SingleOrDefault<AspNetUserRole>("select * from AspNetUserRoles where UserId =@0", id);
        }

        public static AspNetUser GetByID2(string id)
        {
            var db = new BookStoreConnectionDB();
            return db.SingleOrDefault<AspNetUser>("select * from AspNetUsers where Id =@0", id);
        }
    }
}
