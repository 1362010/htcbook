﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStoreConnection;

namespace BookStoreBus.Bus
{
    public class THELOAIBus
    {
        public static IEnumerable<THELOAI> DanhSach()
        {
            var db = new BookStoreConnectionDB();
            return db.Query<THELOAI>("select * from THELOAI ORDER BY MaTheLoai ASC");
        }

        public static IEnumerable<ViewTHELOAI> GetByID(int id)
        {
            var db = new BookStoreConnectionDB();
            return db.Query<ViewTHELOAI>("select * from ViewTHELOAI where MaTheLoai =@0", id);
        }

        public static THELOAI GetByID2(int id)
        {
            var db = new BookStoreConnectionDB();
            return db.SingleOrDefault<THELOAI>("select * from THELOAI where MaTheLoai=@0", id);
        }
    }
}
